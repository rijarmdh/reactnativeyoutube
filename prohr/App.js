/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
  StyleSheet,
  View, Text,
  Image, TouchableHighlight, ScrollView, TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';

export default class App extends React.Component{
  static navigationOptions = {
    headerStyle:{
      backgroundColor: '#fff'
    },

    headerLeft: (
      <TouchableOpacity>
        <Image
          style={{height:22, width:98, color:'#fff', marginLeft:25}}
          source={require('./img/youtube.png')}
        />
      </TouchableOpacity>
    ),
    headerRight: (
      <View style={{flexDirection:'row', marginRight:20}}>
        <TouchableOpacity style={{paddingHorizontal:5}}>
          <Icon name='cast-connected' size={25} color={'#555'}/>
        </TouchableOpacity>
        <TouchableOpacity style={{paddingHorizontal:5}}>
          <Icon name='videocam' size={25} color={'#555'}/>
        </TouchableOpacity>
        <TouchableOpacity style={{paddingHorizontal:5}}>
          <Icon name='search' size={25} color={'#555'}/>
        </TouchableOpacity>
        <TouchableOpacity style={{paddingHorizontal:5}}>
          <Icon name='account-circle' size={25} color={'#555'}/> 
        </TouchableOpacity>
      </View>
    )
  }
  constructor(props){
    super(props)
    this.state = {
      data: []
    }
  };

  //fetching data from api
  componentDidMount(){
    const keyapi = 'AIzaSyAO3rhzNZWwH0kQxly3mvSc-J1gUTg9pms';

    fetch(`https://www.googleapis.com/youtube/v3/search/?key=${keyapi}&channelId=UCMKoGNPOfhxwTc-7XcNuBMw&part=snippet,id&order=date&maxResults=30`)
    .then(res=>res.json())  
    .then(resp=>{
      const videoId = []
      resp.items.forEach(item => {
        videoId.push(item);
      })
      this.setState({
        data: videoId
      })
    })
    .catch(
      err => console.error(err)      
    )    
  };

  render(){
    return(
     <View style={styles.container}>
        <ScrollView>
          <View style={styles.body}>
            {this.state.data.map((item) =>  //Mirip seperti foreach, immutable (tidak berpengaruh terhadap data asli)
              
              <TouchableHighlight //BUTTON
                key={item.id.videoId} //AMBIL IDVIDEO DARI OBJECT ID DIDALAM DATA 
                onPress={() => this.props.navigation.navigate('YoutubeVideo', {youtubeId:item.id.videoId})} // KLIK DAN MENGARAH KE HALAMAN YOUTUBE VIDEO
              >
                <View key={item.id.videoId} style={styles.vids}>
                  <Image
                    key={item.id.videoId}
                    source={{uri: item.snippet.thumbnails.medium.url}}
                    style={{width: 320, height:180}}
                  />
                  <View key={item.id.videoId} style={styles.vidItems}>
                    <Image
                      source={require('./img/got.jpg')}
                      style={{width:40, height: 40, borderRadius: 20, marginRight: 5}}
                    />
                    <Text key={item.id.videoId} style={styles.vidText}>{item.snippet.title}</Text>
                    <Icon key={item.id.videoId} name='more-vert' size={20} color='#555'/>
                  </View>
                </View>
              </TouchableHighlight>
              )
            }
          </View>
        </ScrollView>

        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name='home' size={25} color={'#555'}/> 
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
            
          <TouchableOpacity style={styles.tabItems}>
            <Icon name='trending-up' size={25} color={'#555'}/> 
            <Text style={styles.tabTitle}>Trending</Text>
          
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name='subscriptions' size={25} color={'#555'}/> 
            <Text style={styles.tabTitle}>Subscription</Text>
          </TouchableOpacity>
          
          <TouchableOpacity style={styles.tabItems}>
            <Icon name='local-activity' size={25} color={'#555'}/> 
             <Text style={styles.tabTitle}>Activity</Text>
          </TouchableOpacity>
        
          <TouchableOpacity style={styles.tabItems}>
            <Icon name='video-library' size={25} color={'#555'}/> 
             <Text style={styles.tabTitle}>Library</Text>
          </TouchableOpacity>
        </View>
        
     </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
  },

  body: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 30,
  }, 
  vids: {
    paddingBottom: 30,
    width: 320,
    alignItems: 'center',
    backgroundColor:'#fff',
    justifyContent:'center',
    borderBottomWidth: 0.6,
    borderColor: '#aaa',
  },

  vidItems:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'space-around',
    padding: 10
  }, 

  vidText:{
    padding: 20,
    color: '#000'
  },

  tabBar:{
    backgroundColor: '#fff',
    height: 70,
    flexDirection: 'row',
    justifyContent:'space-around',
    borderTopWidth: 0.5,
    borderColor: '#bbb'
  },

  tabItems: {
    alignItems: 'center',
    justifyContent:'center',
    paddingBottom: 5
  },
  tabTitle:{
    fontSize: 11,
    color:'#333',
    paddingTop:4,
    textDecorationLine: 'underline'
  }
})