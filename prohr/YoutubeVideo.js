import React from 'react';
import Youtube from 'react-native-youtube';
import {
    StyleSheet,
    View
  } from 'react-native';
  import { createStackNavigator } from 'react-navigation';

export default class YoutubeVideo extends React.Component{
    static navigationOption = {
        headerTitle: 'Youtube',
        headerStyle:{
            backgroundColor: '#000'
        },
        headerTitleStyle:{
            color:'#fff'
        }
    }

    render(){
        return(
            <View style={styles.container}> 
                <Youtube
                    /* videoId={this.props.navigation.state.params.youtubeId}   // The YouTube video ID */
                    videoId={this.props.navigation.getParam('youtubeId')}
                    play={true}             // control playback of video with true/false
                    fullscreen={true}       // control whether the video should play in fullscreen or inline
                    loop={false}             // control whether the video should loop when ended
                    apiKey={'AIzaSyAO3rhzNZWwH0kQxly3mvSc-J1gUTg9pms'}
                    onReady={e => this.setState({ isReady: true })}
                    onChangeState={e => this.setState({ status: e.state })}
                    onChangeQuality={e => this.setState({ quality: e.quality })}
                    onError={e => this.setState({ error: e.error })}

                    style={{ alignSelf: 'stretch', height: 300 }}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#fff'
    }
}
)