import React from 'react';
import App from './App';
import YoutubeVideo from './YoutubeVideo';
import { createStackNavigator } from 'react-navigation';
import { AppRegistry, YellowBox } from 'react-native';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);


 const screens = createStackNavigator({
    Home: {screen: App},
    YoutubeVideo: {screen: YoutubeVideo}
  });

AppRegistry.registerComponent('prohr', () => screens);
